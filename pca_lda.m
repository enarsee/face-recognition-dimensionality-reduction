%perform PCA and LDA on train data
PCA_inputTrain =TrainImageDB.Data;
PCA_outputTrain = pca(PCA_inputTrain,240,160);
%LDA_outputTrain = LDA(PCA_outputTrain,6);

% perform PCA and LDA on test data
PCA_inputTest =TestImageDB.Data;
PCA_outputTest = pca(PCA_inputTest,80,160);
%LDA_outputTest = LDA(PCA_outputTest,4);

%knnClassfication with LDA
%accuracy=knn(LDA_outputTrain,TrainImageDB.Label,LDA_outputTest,TestImageDB.Label,160);

%knnClassfication with PCA
accuracy=knn(PCA_outputTrain,TrainImageDB.Label,PCA_outputTest,TestImageDB.Label,160);
%accuracy=knn(TrainImageDB.Data,TrainImageDB.Label,TestImageDB.Data,TestImageDB.Label,160);
%accuracy = svm(LDA_outputTrain,TrainImageDB.Label,LDA_outputTest,TestImageDB.Label,160);