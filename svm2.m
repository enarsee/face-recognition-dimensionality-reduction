%TrainingSet=PCA_outputTrain'; 
%TestSet=PCA_outputTest';

TrainingSet=TrainImageDB.Data'; 
TestSet=TestImageDB.Data';

%TestSet=PCA_outputTest'; 
GroupTrain=TrainImageDB.Label'; 
results = multisvm(TrainingSet, GroupTrain, TestSet);
disp('multi class problem'); 
%disp(results);

count = 0;
    x=TestImageDB.Label';
    for i = 1: 160
        if(results(i) == x(i))
            count=count+1;
        end
    end
    fprintf('Accuracy: %f%%\n',(count/160)*100);