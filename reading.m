%{folder = dir('\\ds3.sce.ntu.edu.sg\xd4$\student\pooja1\My Documents\MachineLearning\att_faces\s1\*.pgm')
%imread
folder = 'database/s';

%Initialize the database
ImageDB.Data = [];
ImageDB.Label = [];
TrainImageDB.Data = [];
TrainImageDB.Label = [];
TestImageDB.Data = [];
TestImageDB.Label = [];

for j = 1:40 %Iterate through folders
    for k = 1:10
        % Create an image filename, and read it in to a variable called image.
        FileName = strcat(folder, num2str(j), '/', num2str(k), '.pgm');
        if exist(FileName, 'file')
            image = imread(FileName);
            %Pre Process the Image
            normalizedImage = preprocess(image);
            %Check the preprocessed image
            if(isempty(normalizedImage) == 0)
                if(k>6)
                    TestImageDB.Data = [TestImageDB.Data normalizedImage];
                    TestImageDB.Label = [TestImageDB.Label j];
                else
                    TrainImageDB.Data = [TrainImageDB.Data normalizedImage];
                    TrainImageDB.Label = [TrainImageDB.Label j];
                end
            end
        end
    end
end