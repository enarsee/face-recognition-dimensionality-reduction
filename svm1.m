


train_label={};
train_cell={}; 
label=unique(TrainImageDB.Label)';
test_mat={}; 
labels={};

P=PCA_outputTrain';
T=PCA_outputTest';
for i=1:40
    train_label{i,1}= (i*ones(1,6));
    labels{i,1}= i*ones(1,4);
    s=6*(i-1)+1;
    e=(6*i);
    st=4*(i-1)+1;
    et=(4*i);
    train_cell{i,1}= P(s:e,:);
    test_mat{i,1}= P(st:et,:);
    
end
[svmstruct] = Train_DSVM(train_cell,train_label); 
[Class_test] = Classify_DSVM(test_mat,label,svmstruct);
%[Cmat,DA]= confusion_matrix(Class_test,labels,{'A','B','C'});