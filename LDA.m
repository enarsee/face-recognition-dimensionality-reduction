function [LDA_output] = LDA(Y,trainIndex)

%function [traindata] = LDA(traindata, trainIndex)
%trainIndex = 6
sumFace = []
meanFaces = []
classNum = 40

%for i=1:(trainIndex*40)
for j = 1:classNum
    %Compute the average face of each person  
    startIndex = 1 + (trainIndex*(j-1));
    endIndex = startIndex + trainIndex - 1;
    meanFace = mean(Y(:,startIndex:endIndex),2)
    meanFaces = [meanFaces meanFace];
end

%Overall mean of all faces
meanAll = sum(meanFaces,2)/40;

SArray = zeros(105,105,40);
%Class covariance matrices 
for m = 1:40
    startIndex = 1 + (trainIndex*(m-1));
    endIndex = startIndex + trainIndex - 1;
    for n = startIndex:endIndex
        S = (cov(Y(:,startIndex:endIndex)'));
        SArray(:,:,m) = S;
    end 
end 

Sw = SArray(:,:,1);
for n = 2:40
    Sw = Sw + SArray(:,:,n);
end 

SBArray = zeros(105,105,40);
for j = 1:40
    SBArray(:,:,j) = trainIndex.*(meanFaces(:,j) - meanAll)*(meanFaces(:,j) - meanAll)';
end

SB = SBArray(:,:,1);
for n = 2:40
    SB = SB + SBArray(:,:,n);
end 

invSw = inv(Sw);
invSw_by_SB = Sw\SB; %invSw * SB;
[V,U,D] = svd(invSw_by_SB);

LDA_output = V(:,1:27)'*Y;



