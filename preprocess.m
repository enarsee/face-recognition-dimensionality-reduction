function [normalizedImage] = preprocess(image)

%Make the image grayscale if it is a truecolor image 
if(size(image, 3) == 3)
    image = rgb2gray(image); 
end

image = histeq(image, 1000);

image = wiener2(image,[3 3]);
%image = edge(image,'canny');


%Use MATLAB's Computer Vision Toolbox
faceDetect = vision.CascadeObjectDetector('FrontalFaceLBP');
%Returns Bounding Box values based on number of objects detected 
%The step(Detector,I) returns Bounding Box value that contains [x,y,Height,Width] of the objects of interest.
boundingBox = step(faceDetect, image);

%Annotate the faces
faceAnnotation = insertObjectAnnotation(image, 'rectangle', boundingBox, 'Face');
%Display the annotated image figure; 
%imshow(faceAnnotation);
title('Face Annotation');


  


%Assign the values of the box into respective coordinates
%The matrix is made up of x-coord y-coord x-length y-length 
if (isempty(boundingBox) == 0)
    x1 = boundingBox(1);
    x2 = boundingBox(3);
    y1 = boundingBox(2);
    y2 = boundingBox(4);
    %Crop the image based on the coordinates 
    cropImage = image(y1: y1 + y2, x1: x1 + x2);
    
    

    %Resizes the image to 64*64 pixels 
    cropImage = imresize(cropImage, [64, 64]);
    
    imshow(cropImage);

    %Reshape the image vector into a single dimension
    %CHECK THIS
    normalizedImage = reshape(cropImage, 64*64, 1);
else 
    %fprintf('Cannot preprocess image...just resizing.\n\n');
    %Resizes the image to 64*64 pixels 
    cropImage = imresize(image, [64, 64]);
    imshow(cropImage);
    %Reshape the image vector into a single dimension
    %CHECK THIS
    normalizedImage = reshape(cropImage, 64*64, 1);
end

