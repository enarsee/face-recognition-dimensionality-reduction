function [PCA_output] = pca(PCA_input,numFaces,d)

f1= double (PCA_input');
f2= double (PCA_input);
faces = f1*f2;

% Perform Singular Value Decomposition
[u,s,v] = svd(faces);

% Pull out eigen values and vectors
eigVals = diag(s);
eigVecs = u;

% The cumulative energy content for the m'th eigenvector is the sum of the energy content across eigenvalues 1:m
for i = 1:numFaces
energy(i) = sum(eigVals(1:i));
end
propEnergy = energy./energy(end);

% Determine the number of principal components required to model 90% of data variance
percentMark = min(find(propEnergy >0.99));

% Pick those principal components
eigenVecs = u(:, 1:d);

W= f2*eigenVecs;
PCA_output=W'*f2;

nearestNeighbours = [1:10]
accuracy = []
%{
for nn = 1:10
    modelknn = fitcknn(Y',TrainImageDB.Label','NumNeighbors',nn);
    %cvmdl = crossval(modelknn);
    %kloss = kfoldLoss(cvmdl);
    label = predict(modelknn,Y');
    count = 0;
    x = TrainImageDB.Label';
    for i = 1:240
        if(label(i) == x(i))
            if(nn==1)
                fprintf('Label:%d x:%d\n',label(i),x(i));
            end
            count=count+1;
        end
    end
    fprintf('Accuracy: %f%%\n',(count/240)*100);
    acc = count/240;
    accuracy = [accuracy acc];
end 
figure
plot(nearestNeighbours,accuracy,'b') %m,c,g,b for diff colors
legend('PCA+knn')
%plotconfusion(label,TrainImageDB.Data)
%[c,cm] = confusion(TrainImageDB.Label',label)
%}
%fprintf('Percentage Correct Classification   : %f%%\n', 100*(1-c));
%fprintf('Percentage Incorrect Classification : %f%%\n', 100*c);