function [accuracy]= svm (trainData,trainLabel,testData,testLabel,totalSamples)

accuracy = []

modelsvm = fitcsvm(trainData',trainLabel');
labels = predict(modelsvm,testData');
count = 0;
    x = testLabel';
    for i = 1:totalSamples
        if(labels(i) == x(i))
            count=count+1;
        end
    end
    fprintf('Accuracy: %f%%\n',(count/totalSamples)*100);
    acc = count/totalSamples;
    accuracy = [accuracy acc]; 
figure
plot(nearestNeighbours,accuracy,'b') %m,c,g,b for diff colors
legend('PCA-LDA+svm')
