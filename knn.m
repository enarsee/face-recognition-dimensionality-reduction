function [accuracy]= knn (trainData,trainLabel,testData,testLabel,totalSamples)
nearestNeighbours = [1:10]
accuracy = []
for nn = 1:10
    modelknn = fitcknn(trainData',trainLabel','NumNeighbors',nn);
    %cvmdl = crossval(modelknn);
    %kloss = kfoldLoss(cvmdl);
    labels = predict(modelknn,testData');
    count = 0;
    x = testLabel';
    for i = 1:totalSamples
        if(labels(i) == x(i))
            count=count+1;
        end
    end
    fprintf('Accuracy: %f%%\n',(count/totalSamples)*100);
    acc = count/totalSamples;
    accuracy = [accuracy acc];
end 
figure
plot(nearestNeighbours,accuracy,'b') %m,c,g,b for diff colors
legend('PCA-LDA+knn')
